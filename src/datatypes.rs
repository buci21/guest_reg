use serde::Serialize;
use serde::Deserialize;
use rocket::http::Status;

extern crate chrono;
use rocket::request::FromRequest;
use rocket::{Outcome, Request};
use crate::auth::validate_token;

//represents a key, needed so that it can implement FromRequest
pub struct JwtToken(String);

//custom error types for different mishaps possible with an authentication key
#[derive(Debug)]
pub enum JwtTokenError {
    BadCount,
    Missing,
    Invalid,
}
//request guard for jwt validation
impl<'a, 'r> FromRequest<'a, 'r> for JwtToken {
    type Error = JwtTokenError;
//returns the ApiKey if it is valid... If no, or more than one key was provided,
//returns with Outcome::Failure
    fn from_request(request: &'a Request<'r>) -> Outcome<Self, (Status, Self::Error), ()> {
        let keys: Vec<_> = request.headers().get("Authorization").collect();
        match keys.len() {
            0 => Outcome::Failure((Status::BadRequest, JwtTokenError::Missing)),
            1 => {
                let mut token = keys[0];
                token = &token[7..];
                let res = validate_token(token);
                match res {
                    Some(_res) => {
                        Outcome::Success(JwtToken(String::from(token)))
                    }
                    None => {
                        Outcome::Failure((Status::BadRequest, JwtTokenError::Invalid))
                    }
                }
            }
            _ => Outcome::Failure((Status::BadRequest, JwtTokenError::BadCount)),
        }
    }
}

//represents one record, containing all data required for a single guest
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegistrationEntry {
    //when received in a request, this is ignored as it is generated by the database
    //when requesting the list of guests, it will be provided with the id
    pub id: Option<i64>,
    pub first_name: String,
    pub last_name: String,
    pub zip_code: Option<String>,
    pub city: Option<String>,
    pub country: Option<String>,
    pub route: Option<String>,
    pub street_number: Option<String>,
    pub date_of_birth: chrono::NaiveDateTime,
    pub passport_number: String,
}

impl Default for RegistrationEntry{
    fn default() -> RegistrationEntry {
        RegistrationEntry{
            id: Some(0),
            first_name: "not provided".to_string(),
            last_name: "not provided".to_string(),
            zip_code: Some("not provided".to_string()),
            city: Some("not provided".to_string()),
            country: Some("not provided".to_string()),
            route: Some("69, Main Rd".to_string()),
            street_number: Some("0".to_string()),
            date_of_birth: chrono::NaiveDateTime::new(
                chrono::NaiveDate::from_ymd(1900, 01, 01),
                chrono::NaiveTime::from_hms(0, 0, 0)),
            passport_number: "not provided".to_string()
        }

    }
}

//needed to respond with multiple RegistrationEntries in a json
#[derive(Serialize, Deserialize)]
pub struct RegistrationEntrySet {
    pub entries: Vec<RegistrationEntry>
}

//sent in request body when logging in or registering
#[derive(Serialize, Deserialize)]
pub struct LoginData {
    pub username: String,
    pub password: String,
}
