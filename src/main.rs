#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket_contrib;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate lazy_static;

extern crate postgres;

use std::env::var;
use postgres::{Connection, TlsMode};
use rocket::{Config, custom};
use rocket::config::Environment;
use rocket::logger::LoggingLevel;

mod endpoints;
mod datatypes;
mod auth;
mod migrations;


lazy_static! {
    /// This is an example for using doc comment attributes
    static ref CONNSTR: String =
        format!(
            "postgres://backend:{}@{}/guestreg",
            var("DB_PASSWORD").unwrap(),
//            "Csodacsibe21",
            var("DB_ADDRESS").unwrap(),
//            "localhosnoicet"
        );
}

lazy_static! {
    static ref SECRET_KEY: String =
        format!(
            "titkos uzenet, szall a szelben"
        );
}

fn main() {
    init_db();

    let cfg = Config::build(Environment::Development)
        .address("0.0.0.0")
        .port(8000)
        .log_level(LoggingLevel::Normal)
        .finalize().unwrap();

    custom(cfg)
        .mount(
            "/",
            routes![
                endpoints::cleardb,
                endpoints::get_latest_guests,
                endpoints::submit,
                endpoints::login,
                endpoints::register
            ],
        )
        .launch();
}

fn init_db() {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .ok();
    match conn {
        Some(conn) => {
            let _ =conn.execute(
                migrations::CREATE_USER_TABLE,
                &[],
            );
            let _ = conn.execute(
                migrations::SET_OWNER_USER_TABLE,
                &[],
            );
            let _ = conn.execute(
                migrations::CREATE_REGISTRATION_TABLE,
                &[],
            );
            let _ = conn.execute(
                migrations::SET_OWNER_REGISTRATION_TABLE,
                &[],
            );
        }

        None => {
            panic!("NO DB CONNECTION")
        }
    }
}