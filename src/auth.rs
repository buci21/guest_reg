use crypto::sha2::Sha256;
use jwt::{Claims, Registered, Token, Header};
use serde::export::Into;
use self::rand::distributions::Alphanumeric;
use self::rand::Rng;
use crate::SECRET_KEY;
extern crate rand;

//generate a new token
//please only call with validated credentials as this function DOES NOT validate it's input
pub fn new_token(
    username: &str,
    _password: &str,
)
    -> Option<String> {
    let header = jwt::header::Header {
        typ: Some(jwt::header::HeaderType::JWT),
        kid: None,
        alg: jwt::header::Algorithm::HS256,
    };
    let claims = Registered {
        iss: Some("guest registration magick".into()),
        sub: Some(username.into()),
        aud: None,
        exp: None,
        nbf: None,
        iat: None,
        jti: Some(rand::thread_rng().sample_iter(Alphanumeric).take(256).collect()),
    };
    let token = Token::new(header, Claims::new(claims));
    //TODO: secret key should be read from file or envvar
    token.signed(SECRET_KEY.as_ref(), Sha256::new()).ok()
}

//validates a token
//used by guard for ApiKey struct (FromRequest trait implemented for ApiKey)
pub fn validate_token(
    token: &str
)
    -> Option<String> {
    let token: jwt::Token<Header, Claims>
        = Token::parse(token).ok()?;
    match
        token.verify(
            //TODO: secret key should be read from file or envvar
            SECRET_KEY.as_ref(),
            Sha256::new(),
        ) {
        true => {
            token.claims.reg.sub
        }
        false => {
            None
        }
    }
}