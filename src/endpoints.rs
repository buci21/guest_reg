use crypto::digest::Digest;
use crypto::sha2::Sha256;
use postgres::{Connection, TlsMode};
use rocket::response::status;
use rocket_contrib::json::{Json, JsonValue};

use crate::auth::new_token;
use crate::CONNSTR;
use crate::datatypes::{JwtToken, LoginData, RegistrationEntrySet};
use crate::datatypes::RegistrationEntry;

//clears the db, to be used only for testing purposes
#[get("/cleardb")]
pub fn cleardb() -> Result<JsonValue, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None).ok();
    match conn {
        None => Err(status::NotFound("No DB connection :(".to_string())),
        Some(conn) => {
            &conn.execute("TRUNCATE public.registration RESTART IDENTITY", &[]).ok();
            Connection::finish(conn).unwrap();
            Ok(json!({
                "result" : "the database was cleaned of all your filthy data... please next time be more careful!"
            }))
        }
    }
}

//if count is 0, retrieves each and every entry from the database for testing purposes, to be disabled later
//with count you can decide how many entries you wish to receive
#[get("/getlatest/<count>")]
pub fn get_latest_guests(
    count: i64,
    _key: JwtToken,
) -> Result<Json<RegistrationEntrySet>, status::NotFound<String>> {
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    let rows;
    if count > 0 {
        rows = conn.query("SELECT id, firstname, lastname, zipcode, city, country, route, streetnumber, dob, ppnumber \
                        FROM public.registration \
                        ORDER BY id DESC LIMIT ($1)",
                          &[&count])
            .map_err(|_| status::NotFound("query got messed up".into()))?;
    } else {
        rows = conn.query("SELECT id, firstname, lastname, zipcode, city, country, route, streetnumber, dob, ppnumber  \
                        FROM public.registration",
                          &[])
            .map_err(|_| status::NotFound("query got messed up".into()))?;
    }

    let mut res = RegistrationEntrySet {
        entries: vec![]
    };

    for i in 0..rows.len() {
        let row = rows.get(i);
        res.entries.push(RegistrationEntry {
            id: row.get(0),
            first_name: row.get(1),
            last_name: row.get(2),
            zip_code: row.get(3),
            city: row.get(4),
            country: row.get(5),
            route: row.get(6),
            street_number: row.get(7),
            date_of_birth: row.get(8),
            passport_number: row.get(9),
        })
    }
    Connection::finish(conn).unwrap();
    Ok(Json(res))
}

//submit a guest registration
//for data structure please see struct RegistrationEntry in datatypes.rs
#[post("/submit", format = "json", data = "<guest_data>")]
pub fn submit(
    guest_data: Json<RegistrationEntry>,
    __key: JwtToken
) -> Result<JsonValue, status::NotFound<String>>
{
    let temp = guest_data.into_inner();
    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    conn.execute(
        "INSERT INTO public.registration\
        (firstname, lastname, zipcode, city, country, route, streetnumber, dob, ppnumber)\
        VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)",
        &[
            &temp.first_name,
            &temp.last_name,
            &temp.zip_code,
            &temp.city,
            &temp.country,
            &temp.route,
            &temp.street_number,
            &temp.date_of_birth,
            &temp.passport_number
        ])
        .map_err(|_| status::NotFound("error while accessing database".into()))?;

    Ok(json!({"result" : "guest registered"}))
}

#[post("/register", format = "json", data = "<login_info>")]
pub fn register(
    login_info: Json<LoginData>
) -> Result<JsonValue, status::NotFound<String>> {
    let guest_data = login_info.into_inner();
    match guest_data.username.chars().all(char::is_alphabetic) {
        true => {
            let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
                .map_err(|_| status::NotFound("No DB connection :(".into()))?;

            let mut hasher = Sha256::new();
            hasher.input_str(guest_data.password.as_ref());
            conn.execute(
                "INSERT INTO public.user (username, hashedpw) \
                        VALUES ($1, $2)",
                &[&guest_data.username, &hasher.result_str()],
            )
                .map_err(|_| status::NotFound("user already exists or something else went wrong".into()))?;

            Ok(json!({"result" : "user succesfully registered"}))
        }
        false => {
            Err(status::NotFound("username can only contain alphanumeric characters".to_string()))
        }
    }
}

//logging in with username and password
//in json response your jwt token is served cold with ice
#[post("/login", format = "json", data = "<login_info>")]
pub fn login(
    login_info: Json<LoginData>,
) -> Result<JsonValue, status::NotFound<String>> {
    let temp = login_info.into_inner();
    let mut hasher = Sha256::new();
    hasher.input_str(temp.password.as_ref());

    let conn = Connection::connect(CONNSTR.as_ref(), TlsMode::None)
        .map_err(|_| status::NotFound("No DB connection :(".into()))?;

    let res = conn.query(
        "SELECT COUNT (*) FROM public.user \
         WHERE username = $1\
         AND hashedpw = $2",
        &[&temp.username, &hasher.result_str()],
    )
        .map_err(|_| status::NotFound("error while accessing the database".into()))?;

    let count: i64 = res.get(0).get(0);
    match count {
        0 => {
            Err(status::NotFound("username or password is incorrect".to_string()))
        }
        1 => {
            match new_token(temp.username.as_str(), temp.password.as_str()) {
                Some(t) => {
                    Ok(json!({"token" : format!("{}", t)}))
                }
                None => {
                    Err(status::NotFound("token generation failed".to_string()))
                }
            }
        }
        _ => {
            Err(status::NotFound("something is messed up with your db... please prepare for self destruct".to_string()))
        }
    }
}