pub const CREATE_USER_TABLE: &str =
r#"CREATE TABLE public."user"
(
    username text COLLATE pg_catalog."default" NOT NULL,
    hashedpw text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT user_pkey PRIMARY KEY (username)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;"#;

pub const SET_OWNER_USER_TABLE: &str =
r#"ALTER TABLE public."user"
    OWNER to backend;"#;

pub const CREATE_REGISTRATION_TABLE: &str =
    r#"CREATE TABLE public.registration
(
    id bigserial NOT NULL,
    firstname text COLLATE pg_catalog."default" NOT NULL,
    lastname text COLLATE pg_catalog."default" NOT NULL,
    zipcode text COLLATE pg_catalog."default" NOT NULL,
    city text COLLATE pg_catalog."default" NOT NULL,
    country text COLLATE pg_catalog."default" NOT NULL,
    route text COLLATE pg_catalog."default" NOT NULL,
    streetnumber text COLLATE pg_catalog."default" NOT NULL,
    dob timestamp without time zone NOT NULL,
    ppnumber text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT registration_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;"#;

pub const SET_OWNER_REGISTRATION_TABLE: &str =
    r#"ALTER TABLE public.registration
    OWNER to backend;"#;