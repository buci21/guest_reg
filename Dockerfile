FROM rustlang/rust:nightly as builder

WORKDIR /app

ADD Cargo.toml .
ADD Cargo.lock .
ADD src/ src/

RUN cargo build --release

FROM ubuntu:latest

WORKDIR /app

COPY --from=builder /app/target/release/guestreg_backend .
ADD rocket.toml .

RUN chmod 777 rocket.toml
RUN chmod +x guestreg_backend

ENTRYPOINT ["./guestreg_backend"]